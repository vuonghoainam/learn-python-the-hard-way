from sys import argv
from os.path import exists

script, from_file, to_file = argv

print("Copying from %s to %s" % (from_file,to_file))

input_raw = open(from_file)
indata = input_raw.read()

print("The input file is %d bytes long" %len(indata))

print("Does the output file exist? %r" %exists(to_file))
print("Ready, hit RETURN to continue or Ctrl C to abort the progress")
input()

output = open(to_file,'w')
output.write(indata)

print("Alright, all done")

output.close()

input_raw.close()