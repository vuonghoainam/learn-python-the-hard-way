def cheese_and_crackers(cheese_count, boxes_of_crackers):
	print("You have %d cheese" % cheese_count)
	print("You have %d boxes of crackers" % boxes_of_crackers)

# We can just give the function numbers directly
cheese_and_crackers(20,30)

# Or we can use var in our script
amount_of_cheese = 10
amout_of_crackers = 50

cheese_and_crackers(amount_of_cheese,amout_of_crackers)

print("We can even do math inside too")
cheese_and_crackers(10+20,5+6)

print("And we can combine the two, var and math")
cheese_and_crackers(amount_of_cheese+100,amout_of_crackers+1000)